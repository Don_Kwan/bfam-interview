package com.example.marketmaker;

import java.io.*;
import java.net.Socket;
import java.nio.channels.SocketChannel;

public class QuoteRequestSubscription implements Runnable, ReferencePriceSourceListener {

    private final BufferedReader input;
    private final BufferedWriter output;
    private final QuoteCalculationEngine engine;
    private final ReferencePriceSource priceSource;
    public final String BUY = "BUY";
    public final String SELL = "SELL";
    public final String DELIMITER = " ";
    private volatile int securityId;
    private volatile boolean isBuy;
    private volatile int quantity;

    public QuoteRequestSubscription(Socket client, QuoteCalculationEngine engine,
                                    ReferencePriceSource priceSource) throws IOException {
        System.out.println("Created quote request subscription for client=" + client.getInetAddress());
        this.engine = engine;
        this.priceSource = priceSource;
        input = new BufferedReader(new InputStreamReader(client.getInputStream()));
        output = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            String line;
            while ((line = input.readLine()) != null) {
                String[] quoteRequest = line.split(DELIMITER);
                if (quoteRequest.length != 3) {
                    sendToClient("quote request is not valid");
                } else {
                    securityId = Integer.parseInt(quoteRequest[0]);
                    isBuy = isBuy(quoteRequest[1]);
                    quantity = Integer.parseInt(quoteRequest[2]);
                    double quote = engine.calculateQuotePrice(securityId,
                            priceSource.get(securityId),
                            isBuy,
                            quantity);
                    if (quote > 0) {
                        sendToClient(String.valueOf(quote));
                    }
                    priceSource.subscribe(this);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            sendToClient(e.getMessage());
        }
    }

    private void sendToClient(String message) {
        try {
            output.write(message);
            output.newLine();
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isBuy(String buySell) {
        if (BUY.equals(buySell)) {
            return true;
        } else if (SELL.equals(buySell)) {
            return false;
        } else {
            throw new IllegalArgumentException("SIDES of quote request is invalid");
        }
    }

    @Override
    public void referencePriceChanged(int securityId, double price) {
        if (securityId == this.securityId) {
            double quote = engine.calculateQuotePrice(securityId,
                    price,
                    isBuy,
                    quantity);
            sendToClient(String.valueOf(quote));
        }
    }
}
