package com.example.marketmaker;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class PriceSourceA implements ReferencePriceSource {

    Map<Integer, Double> securityPriceCache = new ConcurrentHashMap<>(); // should use primitive map to avoid boxing

    private List<ReferencePriceSourceListener> listeners = new CopyOnWriteArrayList<>();

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public double get(int securityId) {
        return securityPriceCache.getOrDefault(securityId, -1d);
    }

    /**
     * Called from price source for new price update
     *
     * @param securityId security id of new price
     * @param newPrice bid price of the security
     */
    @Override
    public void updateSecurityPrice(int securityId, double newPrice) {
        securityPriceCache.put(securityId, newPrice);
        updateRefPriceListener(securityId, newPrice);
    }

    private void updateRefPriceListener(int securityId, double newPrice) {
        for (ReferencePriceSourceListener listener : listeners) {
            listener.referencePriceChanged(securityId, newPrice);
        }
    }


}
