package com.example.marketmaker;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainApp {

    public static void main(String[] args) {
        new MainApp().start();
    }

    private final ExecutorService poolExecutor = Executors.newFixedThreadPool(16);

    public void start() {
        ReferencePriceSource priceSource = new PriceSourceA();
        priceSource.updateSecurityPrice(5, 46.5);
        priceSource.updateSecurityPrice(700, 701.5);
        priceSource.updateSecurityPrice(388, 550.5);
        priceSource.updateSecurityPrice(9988, 247.1);

        int port = 5656;
        poolExecutor.submit(new QuoteRequestServer(5656, priceSource));

        int i = 1;
        Client client1 = new Client(i++, port, "5 BUY 200");
        poolExecutor.submit(client1);
        poolExecutor.submit(new Client(i++, port, "5 BUY 350"));
        poolExecutor.submit(new Client(i++, port, "388 SELL 200"));
        poolExecutor.submit(new Client(i++, port, "9988 BUY 200"));
        poolExecutor.submit(new Client(i++, port, "700 XXX 2000")); // invalid quote request - will be ignored by server
        poolExecutor.submit(new Client(i++, port, "941 BUY 200")); // no price at the beginning

        sleep(2000);
        System.out.println("update securityId 5 price");
        priceSource.updateSecurityPrice(5, 47.5);
        sleep(1000);
        System.out.println("update securityId 5 price");
        priceSource.updateSecurityPrice(5, 48.5);
        sleep(1000);
        System.out.println("update securityId 700 price");
        priceSource.updateSecurityPrice(700, 703.5);
        sleep(1000);
        System.out.println("update securityId 5 price");
        priceSource.updateSecurityPrice(5, 48.5);
        sleep(1000);
        System.out.println("update securityId 388 price");
        priceSource.updateSecurityPrice(388, 555.5);
        sleep(1000);
        System.out.println("update securityId 941 price");
        priceSource.updateSecurityPrice(941, 55.9);

        try {
            sleep(1000);
            client1.sendQuoteRequest("388 BUY 200");
        } catch (IOException e) {
            e.printStackTrace();
        }
        sleep(1000);
        System.out.println("update securityId 388 price");
        priceSource.updateSecurityPrice(388, 580.1);


    }

    private void sleep(int milSec) {
        try {
            Thread.sleep(milSec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public class Client implements Runnable {

        private BufferedWriter out;
        private int clientId;
        private int port;
        private String quoteRequest;

        public Client(int clientId, int port, String quoteRequest) {
            this.clientId = clientId;
            this.port = port;
            this.quoteRequest = quoteRequest;
        }

        public void sendQuoteRequest(String quoteRequest) throws IOException {
            out.write(quoteRequest);
            out.newLine();
            out.flush();
            System.out.println("Client " + clientId + ": send quote request to server: " + quoteRequest);
        }

        @Override
        public void run() {
            try (Socket socket = new Socket("localhost", port)) {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                sendQuoteRequest(quoteRequest);
                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println("Client " + clientId + ": received message=" + line);
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
