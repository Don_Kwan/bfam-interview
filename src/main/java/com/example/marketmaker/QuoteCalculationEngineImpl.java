package com.example.marketmaker;


import java.util.HashMap;
import java.util.Map;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    private final static Map<Integer, Double> securitySpreadMap = new HashMap<>();

    public QuoteCalculationEngineImpl() {
        securitySpreadMap.put(5, 0.5d);
        securitySpreadMap.put(388, 0.5d);
        securitySpreadMap.put(9988, 0.2d);
        securitySpreadMap.put(700, 0.5d);
        securitySpreadMap.put(941, 0.05d);
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        double bidOfferPrice = -1;
        if (referencePrice > 0) {
            if (buy) {
                bidOfferPrice = (referencePrice + securitySpreadMap.get(securityId)) * quantity; // take offer price
            } else {
                bidOfferPrice = referencePrice * quantity;
            }
        }

        try {
            Thread.sleep(1000); // simulate of long time calculation
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bidOfferPrice;
    }
}
