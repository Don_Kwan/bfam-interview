package com.example.marketmaker;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QuoteRequestServer implements Runnable {

    private final ExecutorService poolExecutor = Executors.newCachedThreadPool();
    private final QuoteCalculationEngine engine = new QuoteCalculationEngineImpl();

    private final int port;
    private final ReferencePriceSource priceSource;

    public QuoteRequestServer(int port, ReferencePriceSource priceSource) {
        this.port = port;
        this.priceSource = priceSource;
    }

    @Override
    public void run() {
        try {
            ServerSocket server = new ServerSocket(port);
            System.out.println("Started quote request server with port=" + port);
            while (true) {
                Socket client = server.accept();
                System.out.println("Accepted client connection from " + client.getInetAddress() + ":" + client.getPort());
                poolExecutor.submit(new QuoteRequestSubscription(client, engine, priceSource));
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}
